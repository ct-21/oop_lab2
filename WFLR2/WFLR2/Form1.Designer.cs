﻿namespace WFLR2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.x_tb = new System.Windows.Forms.TextBox();
            this.y_tb = new System.Windows.Forms.TextBox();
            this.z_tb = new System.Windows.Forms.TextBox();
            this.r_tb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "x=";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "y=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "z=";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(42, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 51);
            this.button1.TabIndex = 3;
            this.button1.Text = "Обчислити";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 301);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "r=";
            // 
            // x_tb
            // 
            this.x_tb.Location = new System.Drawing.Point(83, 25);
            this.x_tb.Name = "x_tb";
            this.x_tb.Size = new System.Drawing.Size(196, 27);
            this.x_tb.TabIndex = 5;
            // 
            // y_tb
            // 
            this.y_tb.Location = new System.Drawing.Point(83, 78);
            this.y_tb.Name = "y_tb";
            this.y_tb.Size = new System.Drawing.Size(196, 27);
            this.y_tb.TabIndex = 6;
            // 
            // z_tb
            // 
            this.z_tb.Location = new System.Drawing.Point(83, 137);
            this.z_tb.Name = "z_tb";
            this.z_tb.Size = new System.Drawing.Size(196, 27);
            this.z_tb.TabIndex = 7;
            // 
            // r_tb
            // 
            this.r_tb.Location = new System.Drawing.Point(83, 298);
            this.r_tb.Name = "r_tb";
            this.r_tb.ReadOnly = true;
            this.r_tb.Size = new System.Drawing.Size(196, 27);
            this.r_tb.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.r_tb);
            this.Controls.Add(this.z_tb);
            this.Controls.Add(this.y_tb);
            this.Controls.Add(this.x_tb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Лабораторна робота 2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private Button button1;
        private Label label4;
        private TextBox x_tb;
        private TextBox y_tb;
        private TextBox z_tb;
        private TextBox r_tb;
    }
}