using System;

namespace WFLR2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x = 0;
            double y = 0;
            double z = 0;
            try
            {
                x = double.Parse(x_tb.Text);
                y = double.Parse(y_tb.Text);
                z = double.Parse(z_tb.Text);
            }
            catch (FormatException)
            {
                r_tb.Text = "Error";
            }
            double S = (2 * Math.Cos(Math.Pow(x, 2)) - (1 / 2)) 
                / ((1 / 2) + (Math.Sin(Math.Pow(y, 2-z))) 
                + (Math.Pow(z, 2) / (7 - (z / 3))));
            r_tb.Text = S.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}