﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CALR2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            do
            {
                double x = 0;
                double y = 0;
                double z = 0;
                double result = 0;
                Console.Write("Enter x: ");
                x = Convert.ToDouble(Console.ReadLine());
                Console.Write("Enter y: ");
                y = Convert.ToDouble(Console.ReadLine());
                Console.Write("Enter z: ");
                z = Convert.ToDouble(Console.ReadLine());

                result = (2 * Math.Cos(Math.Pow(x, 2)) - (1 / 2))
                / ((1 / 2) + (Math.Sin(Math.Pow(y, 2 - z)))
                + (Math.Pow(z, 2) / (7 - (z / 3))));

                Console.WriteLine($"Result: {result}");

                Console.Write("Do you want to continue(0 - exit)");
                int choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 0)
                {
                    flag = false;
                }
                Console.WriteLine("\n");
            } while (flag);
        }
    }
}
